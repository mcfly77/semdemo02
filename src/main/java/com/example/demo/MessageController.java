package com.example.demo;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.sap.cloud.servicesdk.xbem.api.MessagingException;

@RestController
@RequestMapping("/")
public class MessageController {

	private static final Logger LOG = Logger.getLogger(MessageController.class.getName());
	private MessageService messageService;

	public MessageController(MessageService messageService) {
		this.messageService = messageService;
	}

	@GetMapping(value = "/messages", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageEvent> getMessages() throws MessagingException {
		if (messageService.initReceiver()) {
			LOG.info(() -> "MessageService receiver is active.");
		}
		return messageService.getReceivedMessageEvents();
	}

	@PostMapping(value = "/messages", consumes = MediaType.TEXT_PLAIN_VALUE)
	public MessageEvent sendMessage(@RequestBody String content, UriComponentsBuilder uriComponentsBuilder) throws MessagingException {
		return messageService.sendContent(content);
	}
}